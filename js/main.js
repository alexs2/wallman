$(document).ready(function(){
	
	/**
	 *		Open drop-down list
	 */
	
	$('.js-button-selection').on('click', function() {
		$(this).toggleClass('is-open');
	});
	
	/**
	 *		Open the catalog
	 */
	
	$('.js-open-catalog').on('click', function() {
		$('.header-panel__drop-down').toggleClass('is-visible');
	});
	
	/**
	 *		Close the previously opened drop-down lists
	 */
	
	$(document).on('click', function(e) {
		if (!$(e.target).closest('.js-button-selection').length) {
			$('.js-button-selection').removeClass('is-open');
		}
		
		if (!$(e.target).closest('.header-panel').length) {
			$('.header-panel__drop-down').removeClass('is-visible');
		}
		e.stopPropagation();
	});
	
	/**
	 *		Fix the menu
	 */
	
	$nav = $('.header-panel');
	$window = $(window);
	$h = $nav.offset().top;
	$(window).scroll(function(){
		if ($(window).scrollTop() > $h) {
			$nav.addClass('fixed');
		} else {
			$nav.removeClass('fixed');
		}
	});

	/**
	 *		Full-screen slider
	 */
	
	var $slickElement = $('.js-approach__list')
	
	$slickElement.on('init reInit beforeChange', function (event, slick, currentSlide, nextSlide) {
		var i = (nextSlide ? nextSlide : 0) + 1;
		$('.js-status').html(i + '<span class="all-pages">/' + slick.slideCount + '</span>');
	});
	
	$slickElement.slick({
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		infinite: false,
		appendArrows: $('.approach__dots'),
		prevArrow: $('.js-approach-arrow__prev'),
		nextArrow: $('.js-approach-arrow__next')
	});
	
	$('.examples__list').slick({
		infinite: true,
		slidesToShow: 1,
		centerMode: true,
		variableWidth: true,
		draggable: false,
		appendArrows: $('.examples__dots'),
		prevArrow: $('.js-examples-arrow__prev'),
		nextArrow: $('.js-examples-arrow__next')
	});
	
	$('.js-reviews-text').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		initialSlide: 2,
		arrows: false,
		fade: true,
		asNavFor: '.js-reviews-icon'
	});
	
	$('.js-reviews-icon').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		initialSlide: 2,
		asNavFor: '.js-reviews-text',
		dots: false,
		arrows: false,
		centerMode: true,
		focusOnSelect: true,
		variableWidth: true,
		infinite: true,
	});
	
	$('.js-banner-list').slick({
		slidesToShow: 1,
		dots: false,
		arrows: false,
		fade: true,
		speed: 300,
		infinite: false,
	});
	
	/**
	 *		Switching banners	
	 */
	
	$('.js-banner-list').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		$('.js-banner-buttom').removeClass('is-active');
		$('.js-banner-buttom').eq(nextSlide).addClass('is-active');
	});
	
	$('.js-banner-buttom').on('click', function() {
		var indexSlide = $(this).index();
		$('.js-banner-buttom').removeClass('is-active');
		$(this).addClass('is-active');
		$('.js-banner-list').slick('slickGoTo', indexSlide, false);
	});
	
	$('.js-select').select2({
		minimumResultsForSearch: Infinity,
		placeholder: 'Назначение перегородок'
	});
	
});